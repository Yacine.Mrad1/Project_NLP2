#!/bin/bash

if [[ $# -eq 0 ]]; then
    path='.'
else
    path="$1"
fi

container_exists=$(docker ps -a -q -f name=tensorflow-container)

if [ -z "$container_exists" ]; then
    echo "Container does not exist, creating..."
    # Create and start a new container with the specified path
    docker run -d -it \
            --name tensorflow-container  \
            -w /workspace/project  \
            --gpus all --ipc=host \
            --ulimit memlock=-1 \
            --ulimit stack=134217728 \
            -v "$path":/workspace/project \
            -p 8888:8888 nvcr.io/nvidia/tensorflow:23.04-tf2-py3 \
                jupyter notebook --ip 0.0.0.0 --no-browser --allow-root
    sleep 5
    docker cp ~/.bashrc tensorflow-container:/root/.bashrc
    docker exec -it tensorflow-container bash -c "pip3 install conlleval datasets seqeval python-crfsuite"
    docker exec -it tensorflow-container bash -c "wget https://raw.githubusercontent.com/sighsmile/conlleval/master/conlleval.py"
else
    echo "Container exists, restarting..."
    docker restart tensorflow-container
    sleep 5
fi


docker logs tensorflow-container | sed 's/hostname/localhost/g'

