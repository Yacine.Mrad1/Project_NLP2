#%% Import libraries and load the dataset

import os
#___ Use GPU if available
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1" #use CPU
os.environ["CUDA_VISIBLE_DEVICES"] = "0" #use GPU0
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from datasets import load_dataset
from collections import Counter
import conlleval
from typing import Dict
import time


from importlib import reload
conlleval = reload(conlleval)


#tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

# To Avoid GPU OOM errors
physical_devices = tf.config.list_physical_devices("GPU")
tf.config.experimental.set_memory_growth(physical_devices[0], True)

print('GPUs = ', tf.config.list_physical_devices('GPU'), '\n',
      'cuda available = ', tf.test.is_built_with_cuda(), '\n',
      'gpu device name = ', tf.test.gpu_device_name(), '\n',
      'visible devices = ', tf.config.get_visible_devices(), '\n',
      'built with gpu support = ', tf.test.is_built_with_gpu_support(), '\n',
      'cuda version = ', tf.sysconfig.get_build_info()['cuda_version'], '\n',
      'cuDNN version = ', tf.sysconfig.get_build_info()['cudnn_version'], '\n'*10)


#___ We will export this data to a tab-separated file format which will be easy to read as a tf.data.Dataset object.
def export_to_file(export_file_path, data):
    with open(export_file_path, "w") as f:
        for record in data:
            ner_tags = record["ner_tags"]
            tokens = record["tokens"]
            if len(tokens) > 0:
                f.write( str(len(tokens)) + "\t" + "\t".join(tokens) + "\t" + "\t".join(map(str, ner_tags)) + "\n")

#____ Load the CoNLL 2003 dataset from the datasets library and process it
conll_data: Dict = load_dataset("conll2003")

os.system('rm -rf data')
os.mkdir("data")
export_to_file("./data/conll_train.txt", conll_data["train"])
export_to_file("./data/conll_val.txt", conll_data["validation"])


#count the number of lines in the file conll_train.txt
num_lines = sum(1 for line in open('./data/conll_train.txt'))
print('Number of lines in the file conll_train.txt = ', num_lines)


#%% Define the model


#___ We will be using a custom loss function that will ignore the loss from padded tokens.
class CustomNonPaddingTokenLoss(keras.losses.Loss):
    def __init__(self, name="custom_ner_loss"):
        super().__init__(name=name)

    def call(self, y_true, y_pred):
        loss_fn = keras.losses.SparseCategoricalCrossentropy(
            from_logits=True, reduction=keras.losses.Reduction.NONE
        )
        loss = loss_fn(y_true, y_pred)
        mask = tf.cast((y_true > 0), dtype=tf.float32)
        loss = loss * mask
        return tf.reduce_sum(loss) / tf.reduce_sum(mask)


#___ Here, we will also be using a custom accuracy metric that will ignore the accuracy from padded tokens.
class CustomNonPaddingTokenAccuracy(keras.metrics.Metric):
    def __init__(self, name="custom_ner_accuracy", **kwargs):
        super().__init__(name=name, **kwargs)
        self.total = self.add_weight(name="total", initializer="zeros")
        self.count = self.add_weight(name="count", initializer="zeros")

    def update_state(self, y_true, y_pred, sample_weight=None):
        """ Calculates how accurate the model's predictions are. """
        mask = tf.cast((y_true > 0), dtype=tf.float32)
        y_true_masked = tf.where(y_true > 0, y_true, 0)  # apply mask to zero-out padding tokens in true labels
        y_true_masked = tf.cast(y_true_masked, dtype=tf.int32)  # cast y_true_masked to int32
        values = tf.cast(tf.equal(y_true_masked, tf.argmax(y_pred, axis=-1, output_type=tf.int32)), dtype=tf.float32)
        values *= mask  # apply mask to zero-out padding tokens in predictions
        self.total.assign_add(tf.reduce_sum(values))
        self.count.assign_add(tf.reduce_sum(mask))

    def result(self):
        return self.total / self.count

    def reset_states(self):
        self.total.assign(0.0)
        self.count.assign(0.0)




#___ We define a TokenAndPositionEmbedding layer
class TokenAndPositionEmbedding(layers.Layer):
    """
    This layer first embeds the tokens and their corresponding positions separately and then adds them together.
    It uses the Keras Embedding layer for both token and position embeddings.
    """
    def __init__(self, maxlen, vocab_size, embed_dim):
        super().__init__()
        self.token_emb = keras.layers.Embedding(
            input_dim=vocab_size, output_dim=embed_dim
        )
        self.pos_emb = keras.layers.Embedding(input_dim=maxlen, output_dim=embed_dim)

    def call(self, inputs):
        maxlen = tf.shape(inputs)[-1]
        positions = tf.range(start=0, limit=maxlen, delta=1)
        position_embeddings = self.pos_emb(positions)
        token_embeddings = self.token_emb(inputs)
        return token_embeddings + position_embeddings


#___ Then we define a TransformerBlock layer
class TransformerBlock(layers.Layer):
    """
    This layer is the main building block of the Transformer architecture.
    It consists of a multi-head self-attention mechanism followed by a position-wise feed-forward network (FFN).
    The output from the multi-head self-attention is added to the original input (residual connection) and then normalized.
    This output is then passed through the FFN, and again, the output from the FFN is added to the input of the FFN
    (another residual connection) and normalized.
    """
    def __init__(self, embed_dim, num_heads, ff_dim, rate=0.1):
        super().__init__()
        self.att = keras.layers.MultiHeadAttention(
            num_heads=num_heads, key_dim=embed_dim
        )
        self.ffn = keras.Sequential(
            [
                keras.layers.Dense(ff_dim, activation="relu"),
                keras.layers.Dense(embed_dim),
            ]
        )
        self.layernorm1 = keras.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = keras.layers.LayerNormalization(epsilon=1e-6)
        self.dropout1 = keras.layers.Dropout(rate)
        self.dropout2 = keras.layers.Dropout(rate)

    def call(self, inputs, training=False):
        attn_output = self.att(inputs, inputs)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(inputs + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        return self.layernorm2(out1 + ffn_output)


#___ Build the NER model class as a keras.Model subclass
class NERModel(keras.Model):
    """
    This is the main model class that combines the above two layers.
    It first applies the TokenAndPositionEmbedding layer to the input, followed by the TransformerBlock.
    The output is then passed through a Dropout layer, a Dense layer with ReLU activation,
    another Dropout layer, and finally a Dense layer with softmax activation to predict the output tags.
    """
    def __init__(
        self, num_tags, vocab_size, maxlen=128, embed_dim=32, num_heads=2, ff_dim=32
    ):
        super().__init__()
        self.embedding_layer = TokenAndPositionEmbedding(maxlen, vocab_size, embed_dim)
        self.transformer_block = TransformerBlock(embed_dim, num_heads, ff_dim)
        self.dropout1 = layers.Dropout(0.1)
        self.ff = layers.Dense(ff_dim, activation="relu")
        self.dropout2 = layers.Dropout(0.1)
        self.ff_final = layers.Dense(num_tags, activation="softmax")

    def call(self, inputs, training=False):
        x = self.embedding_layer(inputs)
        x = self.transformer_block(x)
        x = self.dropout1(x, training=training)
        x = self.ff(x)
        x = self.dropout2(x, training=training)
        x = self.ff_final(x)
        return x


#___ Make the NER label lookup table
#NER labels are usually provided in IOB, IOB2 or IOBES formats. Checkout this link for more information: Wikipedia
#Note that we start our label numbering from 1 since 0 will be reserved for padding. We have a total of 10 labels: 9 from the NER dataset and one for padding.
def make_tag_lookup_table():
    iob_labels = ["B", "I"]
    ner_labels = ["PER", "ORG", "LOC", "MISC"]
    all_labels = [(label1, label2) for label2 in ner_labels for label1 in iob_labels]
    all_labels = ["-".join([a, b]) for a, b in all_labels]
    all_labels = ["[PAD]", "O"] + all_labels
    return dict(zip(range(0, len(all_labels) + 1), all_labels))

mapping = make_tag_lookup_table()
print(mapping)


#___ Get a list of all tokens in the training dataset. This will be used to create the vocabulary.
all_tokens = sum(conll_data["train"]["tokens"], [])
all_tokens_array = np.array(list(map(str.lower, all_tokens)))
counter = Counter(all_tokens_array)
print(len(counter))
num_tags = len(mapping)
vocab_size = 20000
# We only take (vocab_size - 2) most commons words from the training data since
# the `StringLookup` class uses 2 additional tokens - one denoting an unknown
# token and another one denoting a masking token
vocabulary = [token for token, count in counter.most_common(vocab_size - 2)]
# The StringLook class will convert tokens to token IDs
lookup_layer = keras.layers.StringLookup( vocabulary=vocabulary)

#___ Create 2 new Dataset objects from the training and validation data
train_data = tf.data.TextLineDataset("./data/conll_train.txt")
val_data = tf.data.TextLineDataset("./data/conll_val.txt")

#___ Print out one line to make sure it looks good. The first record in the line is the number of tokens. After that we will have all the tokens followed by all the ner tags.
print(list(train_data.take(1).as_numpy_iterator()))

#___ We will be using the following map function to transform the data in the dataset:
def map_record_to_training_data(record):
    record = tf.strings.split(record, sep="\t")
    length = tf.strings.to_number(record[0], out_type=tf.int32)
    tokens = record[1 : length + 1]
    tags = record[length + 1 :]
    tags = tf.strings.to_number(tags, out_type=tf.int64)
    tags += 1
    return tokens, tags


def lowercase_and_convert_to_ids(tokens):
    tokens = tf.strings.lower(tokens)
    return lookup_layer(tokens)


# We use `padded_batch` here because each record in the dataset has a
# different length.
batch_size = 32
train_dataset = (
    train_data.map(map_record_to_training_data)
    .map(lambda x, y: (lowercase_and_convert_to_ids(x), y))
    .padded_batch(batch_size)
)
val_dataset = (
    val_data.map(map_record_to_training_data)
    .map(lambda x, y: (lowercase_and_convert_to_ids(x), y))
    .padded_batch(batch_size)
)

ner_model = NERModel(num_tags, vocab_size, embed_dim=32, num_heads=4, ff_dim=64)

loss = CustomNonPaddingTokenLoss()


#%% Fit the model

#BASELINE !!!!!
#___ Compile and fit the model
#ner_model.compile(optimizer="adam", loss=loss, metrics=["accuracy"]) #OLD one
ner_model.compile(optimizer="adam", loss=loss, metrics=[CustomNonPaddingTokenAccuracy(name="accuracy")])
history = ner_model.fit(train_dataset, validation_data=val_dataset, epochs=10)


#%% Model evaluation and inference

def tokenize_and_convert_to_ids(text):
    tokens = text.split()
    return lowercase_and_convert_to_ids(tokens)

# Sample inference using the trained model
sample_input = tokenize_and_convert_to_ids(
    "eu rejects german call to boycott british lamb"
)
sample_input = tf.reshape(sample_input, shape=[1, -1])
print(sample_input)
output = ner_model.predict(sample_input)
prediction = np.argmax(output, axis=-1)[0]
prediction = [mapping[i] for i in prediction]

# eu -> B-ORG, german -> B-MISC, british -> B-MISC
print(prediction)

conlleval = reload(conlleval)
#___ Metrics calculation
#Here is a function to calculate the metrics. The function calculates F1 score for the overall NER dataset as well as individual scores for each NER tag.
def calculate_metrics(dataset):
    all_true_tag_ids, all_predicted_tag_ids = [], []
    for x, y in dataset:
        output = ner_model.predict(x, verbose=0)
        predictions = np.argmax(output, axis=-1)
        predictions = np.reshape(predictions, [-1])
        true_tag_ids = np.reshape(y, [-1])
        mask = (true_tag_ids > 0) & (predictions > 0)
        true_tag_ids = true_tag_ids[mask]
        predicted_tag_ids = predictions[mask]
        all_true_tag_ids.append(true_tag_ids)
        all_predicted_tag_ids.append(predicted_tag_ids)
    all_true_tag_ids = np.concatenate(all_true_tag_ids)
    all_predicted_tag_ids = np.concatenate(all_predicted_tag_ids)
    predicted_tags = [mapping[tag] for tag in all_predicted_tag_ids]
    real_tags = [mapping[tag] for tag in all_true_tag_ids]
    conlleval.evaluate(real_tags, predicted_tags)

calculate_metrics(val_dataset)


#%%

#____ Plotting the training and validation loss
import matplotlib.pyplot as plt

fh,(ax1,ax2) = plt.subplots(1,2,figsize=(12, 6))

ax1.plot(history.history['loss'], label='Training loss')
ax1.plot(history.history['val_loss'], label='Validation loss')
ax1.set_title('Training and Validation Losses')
ax1.set_xlabel('Epochs')
ax1.set_ylabel('Loss')
ax1.legend()

if('accuracy' in history.history.keys()):
    # Plotting the training and validation accuracy
    ax2.plot(history.history['accuracy'], label='Training Accuracy')
    ax2.plot(history.history['val_accuracy'], label='Validation Accuracy')
    ax2.set_title('Training and Validation Accuracy')
    ax2.set_xlabel('Epochs')
    ax2.set_ylabel('Accuracy')
    ax2.legend()

fh.savefig('neural_bl_training_validation_loss_accuracy.png')


## Conclusions
#In this exercise, we created a simple transformer based named entity recognition model.
#We trained it on the CoNLL 2003 shared task data and got an overall F1 score of around 70%.
#State of the art NER models fine-tuned on pretrained models such as BERT or ELECTRA can easily
#get much higher F1 score -between 90-95% on this dataset owing to the inherent knowledge
#of words as part of the pretraining process and the usage of subword tokenization.

#You can use the trained model hosted on [Hugging Face Hub](https://huggingface.co/keras-io/ner-with-transformers) and try the demo on [Hugging Face Spaces](https://huggingface.co/spaces/keras-io/ner_with_transformers).



#%%

