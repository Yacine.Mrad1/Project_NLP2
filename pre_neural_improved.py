#%%

# language="bash"
# #install the required packages
# pip3 install seqeval
# pip3 install datasets
# pip3 install python-crfsuite
#!pip3 install datasets
#!wget https://raw.githubusercontent.com/sighsmile/conlleval/master/conlleval.py

import lzma
from sklearn.metrics import classification_report, accuracy_score
from seqeval.metrics import classification_report as seqclassify
from datasets import load_dataset
import pycrfsuite
from typing import Dict, List
import random
random.seed(123)

print("All libraries imported successfully.")


def prepare_data(data: List) -> List[List]:
    """
    Prepares the data into the format required for training/testing.
    Each sentence in the data is transformed into a tuple of (tokens, IOB NER Tags).
    Args:
        data (list): The data as a list of dictionaries, where each dictionary has a 'tokens' key and a 'ner_tags' key.
    Returns:
        list: The data prepared as a list of tuples of (tokens, IOB NER Tags).
    Example:
        data = [{'tokens': ['Hello', 'world'], 'ner_tags': [0, 0]}]
        prepare_data(data)
        # Output: [(['Hello', 'world'], [0, 0])]
    """
    sentences = []
    for item in data:
        words = item['tokens']
        labels = item['ner_tags']
        sentences.append((words, labels))
    return sentences


def word_features(sent: List[str], i: int) -> Dict:
    """
    Function to extract word features from a token when given a sentence and the index of the token in said sentence
    Args:
        sent (list): The sentence as a list of words.
        i (int): The index of the word in the sentence to extract features for.
    Returns:
        dict: A dictionary of features for the word.
    Example:
        sent = ['Bon', 'jour']
        i = 1
        word_features(sent, i)
        # Output (example): {'word': 'jour', 'is_first': False, 'is_last': True, ...}
    """
    features = {
        'word': sent[i],
        'is_first': i == 0,
        'is_last': i == len(sent) - 1,
        'is_capitalized': sent[i][0].upper() == sent[i][0],
        'is_all_caps': sent[i].upper() == sent[i],
        'is_all_lower': sent[i].lower() == sent[i],
        'prefix_1': sent[i][0],
        'prefix_2': sent[i][:2],
        'prefix_3': sent[i][:3],
        'prefix_4': sent[i][:4],
        'suffix_1': sent[i][-1],
        'suffix_2': sent[i][-2:],
        'suffix_3': sent[i][-3:],
        'suffix_4': sent[i][-4:],
        'prev_word': '' if i == 0 else sent[i-1],
        'next_word': '' if i == len(sent) - 1 else sent[i+1],
    }
    return features


def conll2003_to_iob(entities: List, encoding: Dict) -> List:
    """
    Function to encode a list of conll2003 entities to the IOB2 Schema
    Args:
        entities (list): List of entity tags
        encoding (dict): Dictionary mapping from the conll2003 schema to IOB2
    Returns:
        list: Converted entity tags according to the IOB2 schema.
    Example:
        Original: [1, 2]
        Encoded: ['B-PER', 'I-PER']
        # Output: ['B-PER', 'I-PER']
    """
    return [encoding[e] for e in entities]


def predict(sentence):
    """
    Predicts the entity tags for the given sentence.
    Args:
        sentence (list): The sentence as a list of words.
    Returns:
        list: The predicted entity tags for each word in the sentence.
    Example:
        sentence = ['Hello', 'world']
        predict(sentence)
        # Output (example): ['O', 'O']
    """
    features_in_sentence = [word_features(sentence, i) for i in range(len(sentence))]
    prediction = tagger.tag(features_in_sentence)
    return prediction


def split_and_process_data(data, max_sentence_length=1000):
    data_ls = []
    maxlen = 0
    for cur_line in data:
        num_splits = (len(cur_line) + max_sentence_length - 1) // max_sentence_length  # Calculate the number of splits needed
        for split_num in range(num_splits):
            start_idx = split_num * max_sentence_length  # Calculate the start index of the current split
            end_idx = min((split_num + 1) * max_sentence_length, len(cur_line))  # Calculate the end index of the current split
            cur_split = cur_line[start_idx:end_idx]  # Get the current split
            cur_line_s = f'{len(cur_split)}\t'
            if len(cur_split) > maxlen:
                maxlen = len(cur_split)
            for cur_word in cur_split:
                cur_line_s += cur_word[0] + '\t'
            for cur_word in cur_split:
                cur_line_s += str(cur_word[-1]) + '\t'
            data_ls.append(cur_line_s.strip())
    return data_ls, maxlen


def convert_wikiner_to_conll(file_path):
    """
    This function converts the WikiNER dataset into the CoNLL 2003 format.
    The output is a list of sentences, where each sentence is a list of [word, ner_tag] pairs.
    Parameters:
        file_path (str): The path to the WikiNER data.
    Returns:
        data_ls (list): A list of sentences, where each sentence is a list of [word, ner_tag] pairs.
    """
    data = []
    maxlen = 0
    tag2int = {
        'O': 0,
        'B-PER': 1,  'I-PER': 2,
        'B-ORG': 3,  'I-ORG': 4,
        'B-LOC': 5,  'I-LOC': 6,
        'B-MISC': 7, 'I-MISC': 8,
    }

    with open(file_path, 'r', encoding='UTF-8') as f:
        sentence = []
        last_tag = 'O'
        for line in f:
            if line.strip() == '':
                if sentence:
                    data.append(sentence)
                    sentence = []
                last_tag = 'O'
            else:
                for word_data in line.strip().split():
                    word, pos, ner = word_data.split('|')
                    # Convert 'I-' to 'B-' for the first token of each entity
                    if ner != 'O':
                        if last_tag == 'O' or last_tag[2:] != ner[2:]:
                            ner = 'B' + ner[1:]
                    last_tag = ner
                    # Update the tag2int dictionary with the new tag if it's not already in the dictionary
                    #if ner not in tag2int:
                        #tag2int[ner] = len(tag2int)
                    sentence.append([word, tag2int[ner]])
        if sentence:
            data.append(sentence)
    #___
    data_ls = []
    for sentence in data:
        sentence_words = []
        sentence_tags = []
        for word, tag in sentence:
            sentence_words.append(word)
            sentence_tags.append(tag)
        data_ls.append((sentence_words, sentence_tags))
    return data_ls


def shuffle_and_split(dataset1, dataset2, test_ratio=0.1):
    # Combine the datasets
    full_dataset = dataset1 + dataset2
    # Shuffle the combined dataset
    random.shuffle(full_dataset)
    # Calculate the number of test examples
    num_test = int(len(full_dataset) * test_ratio)
    # Split the dataset into test and train
    test_set = full_dataset[:num_test]
    train_set = full_dataset[num_test:]
    return train_set, test_set


wiki_data_ls = convert_wikiner_to_conll('aij-wikiner-en-wp3.txt')
print(wiki_data_ls[:10])
train_set, test_set = shuffle_and_split([], wiki_data_ls, test_ratio=0.1)


#%%

#dataset = load_dataset('conll2003')
#train_data: List = dataset['train']
#test_data: List = dataset['test']
#validation_data: List = dataset['validation']

# Pre-processing the dataset to our required format. (Tuples of **(tokens, IOB NER Tags)**)

#___ Conll2003 data is saved in tuples
#___ Tuples contain the tokens in their first element and the TRUE entity tags in their second
#train_sentences = prepare_data(train_data)
#test_sentences = prepare_data(test_data)
#validation_sentences = prepare_data(validation_data)
train_sentences = train_set
test_sentences = test_set

print("Train Data Example: {}".format(train_sentences[0]))
print('Dataset Splits - Train: {}, Test: {} '.format(len(train_sentences), len(test_sentences)))


#%%
label_encoding = {0: 'O', 1: 'B-PER', 2: 'I-PER', 3: 'B-ORG', 4: 'I-ORG', 5: 'B-LOC', 6: 'I-LOC', 7: 'B-MISC', 8: 'I-MISC'}
trainer = pycrfsuite.Trainer(verbose = False)
for sent, labels in train_sentences:
    #Example: sent,labels = ['Nepal', 'offers', 'to', 'talk', 'to', 'Maoist', 'insurgents', '.'] [5, 0, 0, 0, 0, 7, 0, 0]
    # check that there are labels for the sentence
    if labels:
        features_in_sentence = [word_features(sent, i) for i in range(len(sent))]
        #features_in_sentence= [{'word': 'Nepal', 'is_first': True, 'is_last': False, 'is_capitalized': True, 'is_all_c...
        #                       {'word': 'offers', 'is_first': False, 'is_last': False, 'is_capitalized': False, 'is_al...
        #                       {'word': 'to', 'is_first': False, 'is_last': False, 'is_capitalized':
        #                       ...
        prediction = conll2003_to_iob(labels, label_encoding) #prediction= ['B-LOC', 'O', 'O', 'O', 'O', 'B-MISC', 'O', 'O']
        trainer.append(features_in_sentence, prediction)

trainer.set_params({
    'c1': 0.1,
    'c2': 0.01,
    'max_iterations': 100,
    'feature.possible_transitions': True
})

trainer.train('conll2003.crfsuite') #file name to save the model
print('Training Complete...')

#%% Test Evaluation Loop
# After the CRF Training process has finished, the evaluation will be run on the test set,
# making predictions for each sentence in the test set. Predictions will also follow the IOB2 entity tagging schema.

tagger = pycrfsuite.Tagger()
tagger.open('conll2003.crfsuite')

all_true = []
all_pred = []
for sent, labels in test_sentences:
    true_labels = conll2003_to_iob(labels, label_encoding)
    predicted_labels = predict(sent)
    # UNCOMMENT FOR DEBUG
    # print('True: {}'.format(true_labels))
    # print('Predicted: {}'.format(predicted_labels))
    # print('Lengths - True: {} | Predicted: {}'.format(len(true_labels), len(predicted_labels)))
    # print('')
    all_true.append(true_labels)
    all_pred.append(predicted_labels)
print('Evaluation Finished')

## Score Report
# Classic classification report for named entity recognition tasks including **Precision**, **Recall**,
# and the combined **F1-Score** per-label or averages across all labels.
score_report = seqclassify(all_true, all_pred)
print('\n'.join(score_report.splitlines()))

#%%



